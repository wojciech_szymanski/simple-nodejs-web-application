const {Builder, By, Key, until} = require('selenium-webdriver');
 
(async function example() {
  let driver = await new Builder().forBrowser('firefox').build();
  try {
    await driver.get('http://localhost:8080/login');
    await driver.findElement(By.id('login')).sendKeys('user1');
    await driver.findElement(By.id('password')).sendKeys('user1', Key.RETURN);

    // Get all Available cookies
    let oldCookies = await driver.manage().getCookies();

    await driver.manage().deleteAllCookies();
    await driver.get('http://localhost:8080/login');
    await driver.findElement(By.id('login')).sendKeys('user1');
    await driver.findElement(By.id('password')).sendKeys('user1', Key.RETURN);

    await driver.get('http://localhost:8080/change_password');
    await driver.findElement(By.id('password')).sendKeys('user1', Key.RETURN);

    await driver.manage().deleteAllCookies();
    oldCookies.forEach(async cookie => {
        await driver.manage().addCookie({name: cookie.name, value: cookie.value});
    });

    await driver.get('http://localhost:8080/quiz');
    let myUrl = await driver.getCurrentUrl();
    if (myUrl == 'http://localhost:8080/login') {
        console.log("Test 1 passed");
    }
    else {
        console.log("Test 1 failed");
    }

    await driver.get('http://localhost:8080/login');
    await driver.findElement(By.id('login')).sendKeys('user1');
    await driver.findElement(By.id('password')).sendKeys('user1', Key.RETURN);

    //run quiz
    await driver.manage().setTimeouts( { implicit: 1000 } );
    await driver.findElement(By.id('runQuiz1')).click();
    await driver.findElement(By.id('startQuizButton')).click();

    for (let i = 0; i < 3; i++) {
        await driver.findElement(By.id('answer')).sendKeys('123');
        await driver.findElement(By.id('next_button')).click();
    }
    await driver.findElement(By.id('answer')).sendKeys('123');
    await driver.findElement(By.id('end_button')).click();

    myUrl = await driver.getCurrentUrl();
    if (myUrl == 'http://localhost:8080/statistics/1') {
        console.log("Test 2 passed");
    }
    else {
        console.log("Test 2 failed");
    }

    await driver.get('http://localhost:8080/');
    await driver.findElement(By.id('runQuiz1')).click();
    await driver.manage().setTimeouts( { implicit: 200 } );

    let txt = await driver.findElement(By.tagName('body')).getText();

    if(txt.search("ERROR") != -1) {
        console.log("Test 3 passed");
    } else {
        console.log("Test 3 failed")
    }

  } finally {
    await driver.quit();
  }
})();