const express = require("express");
const csrf = require("csurf");
const cookieParser = require("cookie-parser");

const CreateError = require("http-errors");
const session = require("express-session");
const csurf = require("csurf");
const bodyParser = require("body-parser");
const logger = require("morgan");
const sqlite3 = require("sqlite3");
const bcrypt = require("bcrypt")
const connect = require("connect-sqlite3");

const SQLiteStore = connect(session);

function genHash(pass: string): string {
    return bcrypt.hashSync(pass, 10);
}

const db_file_name = "./database.db"
const db = new sqlite3.Database(db_file_name);

// db.serialize(() => {
//     db.run('CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT, login TEXT, password TEXT)');
//     db.run('CREATE TABLE memes (id INTEGER PRIMARY KEY, name TEXT, url TEXT)');
//     db.run('CREATE TABLE quizzes (id INTEGER PRIMARY KEY AUTOINCREMENT, body TEXT)');
//     db.run('CREATE TABLE solutions (id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER, quiz_id INTEGER, body TEXT, FOREIGN KEY (user_id) REFERENCES useres(id), FOREIGN KEY (quiz_id) REFERENCES quizzes (id))');
    
//     db.run('INSERT INTO users (login, password) VALUES ("system", ?)', [genHash('system')]);
//     db.run('INSERT INTO users (login, password) VALUES ("user", ?)', [genHash('user')]);
// });


// const quizzes_data = 
// [
//     {
//         "intro": "Liczyć każdy może.",
//         "data": [
//             {
//                 "question": "4 + 5 = ",
//                 "answer": "9",
//                 "penalty": 20
//             },
//             {
//                 "question": "4 + 2 = ",
//                 "answer": "6",
//                 "penalty": 20
//             },
//             {
//                 "question": "0 * 0 = ",
//                 "answer": "0",
//                 "penalty": 20
//             },
//             {
//                 "question": "1 * 221 = ",
//                 "answer": "221",
//                 "penalty": 20
//             }
//         ]
//     },
//     {
//         "intro": "Liczyć nikt nie może.",
//         "data": [
//             {
//                 "question": "1 + 2 + 3 + 7 = ",
//                 "answer": "13",
//                 "penalty": 12
//             },
//             {
//                 "question": "28 + 28 = ",
//                 "answer": "56",
//                 "penalty": 34
//             },
//             {
//                 "question": "2 * 2 + 2 * 2 + 2 + 2 = ",
//                 "answer": "12",
//                 "penalty": 56
//             },
//             {
//                 "question": "3 * 3 * 3 + 3 + 3 + 3 * 3 + 3 * 3 + 3 - 3 * 3 * 3 + 3 * 3 * 3 = ",
//                 "answer": "54",
//                 "penalty": 78
//             }
//         ]
//     }
// ];

// function addQuiz(id: number, body: string): void {
//     const insertQuiz = 'INSERT INTO quizzes (id, body) VALUES (?, ?)';
//     db.serialize(() => {
//         db.run(insertQuiz, [id, body]);
//     });
// }

// function add_quizzes_to_database() {
//     let i;
//     for (i = 1; i <= quizzes_data.length; i++)
//         addQuiz(i, JSON.stringify(quizzes_data[i - 1]));
// }
// add_quizzes_to_database();

const app = express();
app.use(express.static('public'))
//app.use(cookieParser())
const csrfProtection = csrf({ cookie: true })

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cookieParser('XD'));

app.use(session({
    secret: 'XD',
    resave: false,
    saveUninitialized: true,
    cookie: {
        maxAge: 15 * 60 * 1000
    },
    store: new SQLiteStore({db: ":memory:"})
}));

app.set('view engine', 'pug');
app.use(express.urlencoded({
    extended: true
}));

app.use((req, res, next) => {
    if (!req.session.login) {
        next();
        return;
    }
    const getUser = 'SELECT * FROM users WHERE login = ?';
    db.get(getUser, [req.session.login], (err, row) => {
        if (row === undefined) {
            next();
        } else {
            if (req.session.password && !(req.session.password == row.password)) {
                console.log("Stara sesja, wylogowanie.");
                delete(req.session.password);
                delete(req.session.login);
                delete(req.session.user_id);
            }
            next();
        }
    });
})

app.get('/logout', (req, res) => {
    delete(req.session.login);
    delete(req.session.user_id);
    delete(req.session.password);
    res.redirect('/');
});

app.get('/login', csrfProtection,  (req, res) => {
    res.render('login', {
        message: "Zaloguj się",
        user: req.session.login,
        csrfToken: req.csrfToken()
    });
});

app.get('/change_password', csrfProtection,  (req, res) => {
    res.render('change_password', {
        message: "Zmień hasło",
        user: req.session.login,
        csrfToken: req.csrfToken()
    });
});

app.post('/change_password_form', csrfProtection, (req, res) => {
    const getUser = 'SELECT * FROM users WHERE login = ?';
    if (!req.session.login)
        return;
    db.get(getUser, [req.session.login], (err, row) => {
        if (row === undefined) {
            console.log("Nie ma takiego usera : (");
            return res.redirect('/');
        } else {
            const changePass = 'UPDATE users SET password = ? WHERE login = ?'
            db.run(changePass, [genHash(req.body.password), req.session.login], () => {
                return res.redirect('/');
            });
        }
    });
})

app.post('/loginform', csrfProtection, (req, res, next) => {
    const getUser = 'SELECT * FROM users WHERE login = ?';
    db.get(getUser, [req.body.login], (err, row) => {
        if (row === undefined) {
            console.log("Nie ma takiego usera : (");
            next();
        } else {
            if (!bcrypt.compareSync(req.body.password, row.password)) {
                console.log("Nieprawidlowe haslo!");
                next();
            } else {
                console.log("Jest user!");
                req.session.login = req.body.login;
                req.session.user_id = row.id;
                req.session.password = row.password;
                return res.redirect('/');
            }
        }
    });
});

app.get('/all_quizzes', function(req, res) {
    const getQuizzes = 'SELECT * FROM quizzes';
    db.all(getQuizzes, (err, rows) => {
        const result = [];
        rows.forEach(quiz => {
            result.push(JSON.parse(quiz.body));
        });
        res.json(result);
    })
})

function check_permissions(req, res, next) {
    if (!req.session.user_id) {
        res.redirect("/login");
        return;
    }
    let quizId = parseInt(req.params.quizId, 10);
    if (!quizId)
        quizId = parseInt(req.query.quiz_id, 10);
    if (!quizId) {
        res.render('index', {
            error: "ERROR, bad request",
            user: req.session.user_id
        })
        return;
    }
    //console.log(quizId);
    const getSolution = 'SELECT * FROM solutions WHERE quiz_id = ? and user_id = ?'
    db.get(getSolution, [quizId, req.session.user_id], (err, row) => {
        if (!row)
            next();
        else
            res.render('index', {
                error: "ERROR, already solved",
                user: req.session.user_id
            })
    })
}

app.get('/quiz/:quizId', check_permissions, (req, res) => {
    const getQuiz = 'SELECT * FROM quizzes WHERE id = ?';
    const quizId = parseInt(req.params.quizId, 10);
    req.session.solving = quizId;
    const currentDate = new Date();
    req.session.start_time = currentDate.getTime();
    db.get(getQuiz, [quizId], (err, row) => {
        if (row === undefined)
            return;
        res.json(JSON.parse(row.body));
    })
})

app.post('/quiz_solution/:quizId', (req, res) => {
    const quizId = parseInt(req.params.quizId, 10);
    if (req.session.user_id && req.session.solving == quizId) {
        const currentDate = new Date();
        let t1 = req.session.start_time, t2 = currentDate.getTime();
        delete(req.session.solving);
        delete(req.session.start_time);
        console.log(t1 + " " + t2);

        const getQuiz = "SELECT * FROM quizzes WHERE id = ?";
        let result = 0;
        db.get(getQuiz, [quizId], (err, row) => {
            if (row) {
                const myQuiz = JSON.parse(row.body);
                let tmp_result = (t2 - t1) / 1000;
                for (let i = 0; i < myQuiz.data.length; i++) {
                    if (req.body.answers[i] != myQuiz.data[i].answer)
                        tmp_result += myQuiz.data[i].penalty;
                }
                console.log("Result: " + tmp_result.toFixed(1));
                result = tmp_result;
            }
            else {
                result = 1000000000;
            }

            const insertStats = `INSERT INTO solutions (user_id, quiz_id, body) VALUES (?, ?, ?)`
            
            db.run(insertStats, [req.session.user_id, quizId, JSON.stringify({
                answers: req.body.answers,
                stats: req.body.stats,
                t1: t1,
                t2: t2,
                result
            })], () => {
                res.sendStatus(200);
            })

        })
    }
})

app.get('/statistics/:quizId', (req, res) => {
    const quizId = parseInt(req.params.quizId, 10);
    if (!req.session.user_id) {
        res.redirect('/login');
        return;
    }
    const getMyResult = "SELECT * FROM solutions WHERE user_id = ? and quiz_id = ?";
    const getQuiz = "SELECT * FROM quizzes WHERE id = ?";
    const getAllResults = "SELECT * FROM solutions WHERE quiz_id = ?";
    db.get(getMyResult, [req.session.user_id, quizId], (err, row) => {
        if (row) {
            const myResult = JSON.parse(row.body);
            console.log(myResult);
            console.log(quizId);
            db.get(getQuiz, [quizId], (err, row) => {
                if (row) {
                    const myQuiz = JSON.parse(row.body);
                    console.log(myQuiz);
                    db.all(getAllResults, [quizId], (err, rows) => {
                        if (rows) {
                            const allAnswers = rows;
                            const allAnswersParsed = [];
                            allAnswers.forEach(row => {
                                allAnswersParsed.push(JSON.parse(row.body));
                            });
                            allAnswersParsed.sort((a, b) => a.result - b.result);
                            const bestStats = [];
                            for (let i = 0; i < 5 && i < allAnswersParsed.length; i++) {
                                bestStats.push(allAnswersParsed[i]);
                            }
                            const averageTimes = [];
                            for (let i = 0; i < myQuiz.data.length; i++) {
                                let sum = 0, cnt = 0;
                                for (let j = 0; j < allAnswersParsed.length; j++) {
                                    if (allAnswersParsed[j].answers[i] == myQuiz.data[i].answer) {
                                        sum += allAnswersParsed[j].stats[i] * (allAnswersParsed[j].t2 - allAnswersParsed[j].t1);
                                        cnt++;
                                    }
                                }
                                if (cnt == 0)
                                    cnt = 1;
                                sum /= cnt;
                                averageTimes.push(sum);
                            }
                            res.render('statistics', {
                                user: req.session.login,
                                bestStats,
                                averageTimes,
                                myResult,
                                myQuiz
                            });
                        } else {
                            res.redirect('/');
                        }
                    })
                } else {
                    res.redirect('/');
                }
            })
        } else {
            res.redirect('/');
        }
    })
})

app.get('/my_results', csrfProtection, (req, res) => {
    if (!req.session.user_id) {
        res.redirect('/login');
        return;
    }
    console.log(req.session.login)
    const getResults = 'SELECT * FROM solutions WHERE user_id = ?';
    db.all(getResults, [req.session.user_id], (err, rows) => {
        res.render('my_results', {
            quizzes: rows,
            user: req.session.login
        });
    })
})

app.get('/', function(req, res) {
    res.render('index', {
        user: req.session.login
    });
});

app.get('/quiz', check_permissions, (req, res) => {
    if (req.session.user_id)
        res.render('quiz', {
            user: req.session.login
        });
    else
        res.redirect('/login');
})

app.listen(8080);