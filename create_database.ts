import * as sqlite from 'sqlite3';

const bcrypt = require("bcrypt")

const db_file_name = "./database.db"

let db = new sqlite.Database(db_file_name);

const quizzes_data = 
[
    {
        "intro": "Liczyć każdy może.",
        "data": [
            {
                "question": "4 + 5 = ",
                "answer": "9",
                "penalty": 20
            },
            {
                "question": "4 + 2 = ",
                "answer": "6",
                "penalty": 20
            },
            {
                "question": "0 * 0 = ",
                "answer": "0",
                "penalty": 20
            },
            {
                "question": "1 * 221 = ",
                "answer": "221",
                "penalty": 20
            }
        ]
    },
    {
        "intro": "Liczyć nikt nie może.",
        "data": [
            {
                "question": "1 + 2 + 3 + 7 = ",
                "answer": "13",
                "penalty": 12
            },
            {
                "question": "28 + 28 = ",
                "answer": "56",
                "penalty": 34
            },
            {
                "question": "2 * 2 + 2 * 2 + 2 + 2 = ",
                "answer": "12",
                "penalty": 56
            },
            {
                "question": "3 * 3 * 3 + 3 + 3 + 3 * 3 + 3 * 3 + 3 - 3 * 3 * 3 + 3 * 3 * 3 = ",
                "answer": "54",
                "penalty": 78
            }
        ]
    }
];

function addQuiz(id: number, body: string): void {
    const insertQuiz = 'INSERT INTO quizzes (id, body) VALUES (?, ?)';
    db.serialize(() => {
        db.run(insertQuiz, [id, body]);
    });
}

function add_quizzes_to_database() {
    let i;
    for (i = 1; i <= quizzes_data.length; i++)
        addQuiz(i, JSON.stringify(quizzes_data[i - 1]));
}

function genHash(pass: string): string {
    return bcrypt.hashSync(pass, 10);
}

db.serialize(() => {
    db.run('CREATE TABLE users (id INTEGER PRIMARY KEY AUTOINCREMENT, login TEXT, password TEXT)');
    db.run('CREATE TABLE memes (id INTEGER PRIMARY KEY, name TEXT, url TEXT)');
    db.run('CREATE TABLE quizzes (id INTEGER PRIMARY KEY AUTOINCREMENT, body TEXT)');
    db.run('CREATE TABLE solutions (id INTEGER PRIMARY KEY AUTOINCREMENT, user_id INTEGER, quiz_id INTEGER, body TEXT, FOREIGN KEY (user_id) REFERENCES useres(id), FOREIGN KEY (quiz_id) REFERENCES quizzes (id))');
    
    db.run('INSERT INTO users (login, password) VALUES ("user1", ?)', [genHash('user1')]);
    db.run('INSERT INTO users (login, password) VALUES ("user2", ?)', [genHash('user2')]);

    add_quizzes_to_database();

});

db.close();