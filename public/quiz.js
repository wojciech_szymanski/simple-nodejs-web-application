var quizNumberElement = document.getElementById("quiz_number");
var questionNumberElement = document.getElementById("question_number");
var introElement = document.getElementById("intro");
var solveQuizElement = document.getElementById("solve_quiz");
var resultsElement = document.getElementById("results");
var questionElement = document.getElementById("question");
var quizDescriptionElement = document.getElementById("quiz_description");
var penaltyElement = document.getElementById("penalty");
var answerElement = document.getElementById("answer");
var resultsListElement = document.getElementById("results_list");
var bestResultsListElement = document.getElementById("best_results");
var noResultsElement = document.getElementById("no_results");
var resultElement = document.getElementById("result");
var timerElement = document.getElementById("timer");
var sideElement = document.getElementById("side");
var nextButton = document.getElementById("next_button");
var prevButton = document.getElementById("prev_button");
var endButton = document.getElementById("end_button");
var urlParams = new URLSearchParams(window.location.search);
var quizNumber = urlParams.get("quiz_id");
var timer_id;
var currentQuestion = 1;
var currentQuiz = 1;
var penalty = [];
var answers = [];
var time;
var result;
function isQuizResultsList(x) {
    if (!Array.isArray(x))
        return false;
    if (x.every(function (e) { return (typeof e === "object" && e.result); }))
        return true;
    return false;
}
var quiz_data;
/*[
    {
        "intro": "Liczyć każdy może.",
        "data": [
            {
                "question": "4 + 5 = ",
                "answer": "9",
                "penalty": 20
            },
            {
                "question": "4 + 2 = ",
                "answer": "6",
                "penalty": 20
            },
            {
                "question": "0 * 0 = ",
                "answer": "0",
                "penalty": 20
            },
            {
                "question": "1 * 221 = ",
                "answer": "221",
                "penalty": 20
            }
        ]
    },
    {
        "intro": "Liczyć nikt nie może.",
        "data": [
            {
                "question": "1 + 2 + 3 + 7 = ",
                "answer": "13",
                "penalty": 12
            },
            {
                "question": "28 + 28 = ",
                "answer": "56",
                "penalty": 34
            },
            {
                "question": "2 * 2 + 2 * 2 + 2 + 2 = ",
                "answer": "12",
                "penalty": 56
            },
            {
                "question": "3 * 3 * 3 + 3 + 3 + 3 * 3 + 3 * 3 + 3 - 3 * 3 * 3 + 3 * 3 * 3 = ",
                "answer": "54",
                "penalty": 78
            }
        ]
    }
];*/
function prepare_page() {
    if (quizNumber == null) {
        window.open("/", "_self");
        return;
    }
    currentQuiz = parseInt(quizNumber);
    if (currentQuiz < 1) {
        window.open("/", "_self");
        return;
    }
    quizNumberElement.innerText = quizNumber;
    introElement.classList.remove("hidden");
    solveQuizElement.classList.add("hidden");
    resultsElement.classList.add("hidden");
    sideElement.classList.add("hidden");
}
function checkEndButton() {
    save_answer();
    for (var i = 0; i < quiz_data.data.length; i++) {
        if (answers[i].length == 0) {
            endButton.disabled = true;
            return;
        }
    }
    endButton.disabled = false;
}
function checkNextButton() {
    if (currentQuestion === quiz_data.data.length)
        nextButton.disabled = true;
    else
        nextButton.disabled = false;
}
function checkPrevButton() {
    if (currentQuestion === 1)
        prevButton.disabled = true;
    else
        prevButton.disabled = false;
}
function download_quiz_data() {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            quiz_data = JSON.parse(xmlHttp.responseText);
            answers = [];
            penalty = [];
            for (var i = 0; i < quiz_data.data.length; i++) {
                answers.push('');
                penalty.push(0);
            }
            currentQuestion = 1;
            answerElement.addEventListener("input", checkEndButton);
            load_best_results();
            time = 0;
            reload_timer();
            load_question();
            start_timer();
        }
    };
    xmlHttp.open("GET", "/quiz/" + currentQuiz, true); // true for asynchronous
    xmlHttp.setRequestHeader('Content-Type', 'application/json');
    xmlHttp.send(null);
}
function load_question() {
    questionNumberElement.innerText = currentQuestion.toString();
    penaltyElement.innerText = quiz_data.data[currentQuestion - 1].penalty.toString();
    questionElement.innerText = quiz_data.data[currentQuestion - 1].question;
    quizDescriptionElement.innerText = quiz_data.intro;
    answerElement.value = answers[currentQuestion - 1];
    checkEndButton();
    checkNextButton();
    checkPrevButton();
}
function timer() {
    penalty[currentQuestion - 1]++;
    time++;
    reload_timer();
}
function start_timer() {
    timer_id = window.setInterval(timer, 100);
}
function stop_timer() {
    clearInterval(timer_id);
}
function reload_timer() {
    timerElement.innerText = (time / 10.0).toFixed(1);
}
function start() {
    introElement.classList.add("hidden");
    solveQuizElement.classList.remove("hidden");
    resultsElement.classList.add("hidden");
    sideElement.classList.remove("hidden");
    download_quiz_data();
    //load_question();
    //start_timer();
    //rest is in ajax callback
}
function save_answer() {
    answers[currentQuestion - 1] = answerElement.value;
}
function next() {
    save_answer();
    if (currentQuestion < quiz_data.data.length)
        currentQuestion++, load_question();
}
function prev() {
    save_answer();
    if (currentQuestion > 1)
        currentQuestion--, load_question();
}
function scale(arr) {
    var suma = 0;
    arr.forEach(function (x) {
        suma += x;
    });
    if (suma == 0)
        suma = 1;
    var res = [];
    arr.forEach(function (x) {
        res.push(x / suma);
    });
    return res;
}
function end() {
    stop_timer();
    save_answer();
    introElement.classList.add("hidden");
    solveQuizElement.classList.add("hidden");
    resultsElement.classList.remove("hidden");
    sideElement.classList.add("hidden");
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            window.open("/statistics/" + currentQuiz, "_self");
        }
    };
    xmlHttp.open("POST", "/quiz_solution/" + currentQuiz, true);
    xmlHttp.setRequestHeader("Content-type", "application/json");
    xmlHttp.send(JSON.stringify({ "answers": answers, "stats": scale(penalty) }));
    /*resultsListElement.innerText = "";
    result = 0;
    for (let i = 0; i < quiz_data[currentQuiz - 1].data.length; i++) {

        let li = document.createElement('li');

        if (answers[i] === quiz_data[currentQuiz - 1].data[i].answer)
            li.classList.add("correct");
        else
            li.classList.add("wrong"), penalty[i] += quiz_data[currentQuiz - 1].data[i].penalty * 10;

        li.innerHTML += "Question: " + quiz_data[currentQuiz - 1].data[i].question + "<br>" +
            "Your answer: " + answers[i] + "<br>" +
            "Correct answer: " + quiz_data[currentQuiz - 1].data[i].answer + "<br>" +
            "Total question penalty: " + (penalty[i] / 10).toFixed(1);
        
        resultsListElement.appendChild(li);

        result += penalty[i];
    }
    resultElement.innerText = (result / 10).toFixed(1);*/
}
function quit_quiz() {
    stop_timer();
    prepare_page();
}
function get_saved_data() {
    var saved_string = window.localStorage.getItem(currentQuiz.toString());
    var saved_data;
    if (saved_string != null)
        saved_data = JSON.parse(saved_string);
    if (!isQuizResultsList(saved_data))
        saved_data = [];
    return saved_data;
}
function insert_new_result(x) {
    var saved_data = get_saved_data();
    saved_data.push(x);
    saved_data.sort(function (a, b) { return a.result - b.result; });
    saved_data = saved_data.slice(0, 10);
    window.localStorage.setItem(currentQuiz.toString(), JSON.stringify(saved_data));
}
function save_only_result() {
    insert_new_result({ "result": result });
    prepare_page();
}
function save_with_stats() {
    insert_new_result({ "result": result, "stats": penalty });
    prepare_page();
}
function load_best_results() {
    var saved_data = get_saved_data();
    bestResultsListElement.innerText = '';
    saved_data.forEach(function (element) {
        var li = document.createElement('li');
        li.innerHTML += (element.result / 10).toFixed(1);
        bestResultsListElement.appendChild(li);
    });
    if (saved_data.length == 0)
        noResultsElement.classList.remove("hidden");
    else
        noResultsElement.classList.add("hidden");
}
function go_back() {
    window.open("/", "_self");
}
prepare_page();
